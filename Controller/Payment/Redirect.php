<?php

namespace Payzos\PayzosMagento\Controller\Payment;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order;

class Redirect extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $log;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    private $orderRepository;

    /**
     * @var \Payzos\PayzosMagento\Helper\Data
     */
    private $payzos_magento_helper;

    /**
     *
     * @var \Payzos\PayzosMagento\Block\Redirect
     */
    private $block;

    /**
     *
     * @var \Payzos\PayzosMagento\Lib\Payzos
     */
    private $payzos;

    /**
     *
     * @return  void
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Quote\Model\QuoteRepository $quoteRepository,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Payzos\PayzosMagento\Helper\Data $payzos_magento_helper,
        \Payzos\PayzosMagento\Lib\Payzos $payzos,
        \Payzos\PayzosMagento\Block\Redirect $block
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->checkoutSession = $checkoutSession;
        $this->log = $logger;
        $this->orderRepository = $orderRepository;
        $this->payzos_magento_helper = $payzos_magento_helper;
        $this->payzos = $payzos;
        $this->block = $block;

        parent::__construct($context);
    }

    /**
     * @return Magento\Framework\App\Action\Action::_redirect
     */
    public function execute()
    {
        if (empty($this->checkoutSession->getData('last_success_quote_id'))) {
            return $this->_redirect('checkout/cart');
        }

        $this->log->info('ORDER_ID: ' . $this->checkoutSession->getLastRealOrder()->getId());
        $payemntData = $this->block->getPaymentData();
        $quoteData = $this->quoteRepository->get(
            $this->checkoutSession->getData('last_success_quote_id')
        );
        $make_payment = $this->payzos->make_payment(
            $payemntData['wallet_hash'],
            $quoteData->getData()["grand_total"],
            $payemntData['currency_code'],
            $this->block->back_url($this->checkoutSession->getLastRealOrder()->getId())
        );
        // if something goes wrong
        if (!$make_payment) {
            echo "<script>";
            echo "setTimeout(()=>{";
            echo "window.location.replace('/checkout/cart');";
            echo "},3000);";
            echo "</script>";
            die("an error occure. try another payment ( you will redirect in 5 sec )");
        }
        $orderModel = $this->orderRepository->get(
            $this->checkoutSession->getLastRealOrder()->getId()
        );
        $orderModel
            ->setState(Order::STATE_NEW)
            ->setStatus($this->payzos_magento_helper->getGeneralConfig('status_order_placed'));

        return $this->_redirect($make_payment["url"]);
    }
}
