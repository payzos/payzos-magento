<?php

namespace Payzos\PayzosMagento\Controller\Payment;

use Magento\Framework\App\Action\Context;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Action\HttpGetActionInterface;

class BackUrl extends \Magento\Framework\App\Action\Action implements HttpGetActionInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;

    /**
     * [$resultJsonFactory description]
     *
     * @var [type]
     */
    private $resultJsonFactory;

    /**
     * @var  \Magento\Framework\ObjectManagerInterface
     */

    protected $_objectManager;

    /**
     * [$configResource description]
     *
     * @var [type]
     */
    private $configResource;

    /**
     * @var \Magento\Framework\App\Config\MutableScopeConfigInterface
     */
    private $config;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    /**
     * @var
     *
     */
    private $log;

    /**
     * @var \Magento\Sales\Model\OrderRepository
     */
    private $orderRepository;

    /**
     * @var \Payzos\PayzosMagento\Helper\Data
     */
    private $payzos_magento_helper;

    /**
     *
     * @var [type]
     */
    private $block;

    /**
     *
     * @var [type]
     */
    private $payzos;

    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Config\MutableScopeConfigInterface $config
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\OrderRepository $orderRepository
     * @param \Payzos\PayzosMagento\Helper\Data $payzos_magento_helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Config\MutableScopeConfigInterface $config,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Payzos\PayzosMagento\Logger\Logger $logger,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Payzos\PayzosMagento\Helper\Data $payzos_magento_helper,
        \Payzos\PayzosMagento\Lib\Payzos $payzos,
        \Payzos\PayzosMagento\Block\Redirect $block
    ) {
        $this->_coreRegistry = $coreRegistry;
        $this->resultJsonFactory = $jsonFactory;
        $this->config = $config;
        $this->checkoutSession = $checkoutSession;
        $this->log = $logger;
        $this->orderRepository = $orderRepository;
        $this->payzos_magento_helper = $payzos_magento_helper;
        $this->payzos = $payzos;
        $this->block = $block;
        $this->_objectManager = $objectmanager;

        parent::__construct($context);
    }

    /**
     * Undocumented function
     *
     * @param [type] $_message
     * @return void
     */
    private function error_api_output($_message)
    {
        $output = [];
        $output['ok'] = false;
        $output['message'] = $_message;
        $resultJson = $this->resultJsonFactory->create();
        return $resultJson->setData($output);
    }

    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        echo "FUCK";
        return $this->execute();
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function execute()
    {
        $data = file_get_contents('php://input');
        if (empty($data) || !is_string($data)) {
            return $this->error_api_output('unvalid request');
        }
        $data = json_decode($data, true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            return $this->error_api_output('unvalid input');
        }
        if (!isset($data['payment_id']) || !isset($_GET['order_id'])) {
            return $this->error_api_output('fill values');
        }
        $payment_id = filter_var($data['payment_id'], FILTER_SANITIZE_STRING);
        $order_id = filter_var($_GET['order_id'], FILTER_SANITIZE_STRING);
        if (!is_numeric($order_id) || intval($order_id) == 0) {
            return $this->error_api_output('unvalid order_id');
        }
        $order_id = (int) $order_id;
        $api_response = $this->payzos->get_payment($payment_id);
        if (!$api_response) {
            return $this->error_api_output('wrong payment');
        }
        if (!isset($api_response['status']) || $api_response['status'] !== 'approved') {
            return $this->error_api_output('wrong payment');
        }

        list($payzos_total, $payzos_currency) = explode(" ", $api_response["real_amount"]);
        if ($order = $this->orderRepository->get($order_id)) {
            if ($order->getState() == Order::STATE_NEW || $order->getState() == 'pending') {
                if ($payzos_currency == $order->getBaseCurrencyCode()) {
                    if ($payzos_total == $order->getBaseGrandTotal()) {
                        $this->updateStatus($order, $payment_id);
                        $order->save();
                        $output = [
                            "redirect_url" => $this->get_success_page_url($payment_id),
                        ];
                        $resultJson = $this->resultJsonFactory->create();
                        return $resultJson->setData($output);
                    }
                }
            }
        }
    }

    private function get_success_page_url($_payment_id)
    {
        $url = $this->urlBuilder->getUrl('payzosmagento/checkout/success');
        if (strpos($url, "?")) {
            $url .= "&payment_id =" . $_payment_id;
        } else {
            $url .= "?payment_id =" . $_payment_id;
        }
        return $url;
    }

    /**
     *
     *
     * @param [type] $order
     * @param [type] $_payment_id
     * @return void
     */
    private function updateStatus($order, $_payment_id)
    {
        $order
            ->setState($this->helper->getGeneralConfig('status_order_paid'))
            ->setStatus($this->helper->getGeneralConfig('status_order_paid'));
        $order->addCommentToStatusHistory(
            "Payzos payment_id : " . $_payment_id,
            $this->helper->getGeneralConfig('status_order_paid')
        );
        $order->save();
        if ($order->canInvoice()) {
            $invoice = $this->_objectManager
                ->create('\Magento\Sales\Model\Service\InvoiceService')
                ->prepareInvoice($order);
            $invoice->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
            $invoice->register();
            //$invoice->save();

            $this->logInfo("created invoice " . $invoice->getId(), $order);

            $transactionSave = $this->_objectManager
                ->create('\Magento\Framework\DB\Transaction')
                ->addObject($invoice)
                ->addObject($order);
            $transactionSave->save();
            $order
                ->addStatusHistoryComment(
                    __('Notified customer about invoice #%1.', $invoice->getId())
                )
                ->setIsCustomerNotified(true)
                ->save();
            $this->_objectManager
                ->create(\Magento\Sales\Api\InvoiceManagementInterface::class)
                ->notify($invoice->getEntityId());
        }
    }

    /**
     * @param      $msg
     * @param null $order
     */
    private function logInfo($msg, $order = null)
    {
        if ($this->helper->getGeneralConfig('debug')) {
            $messsageString = '';
            if ($order !== null) {
                $messsageString = 'Order ID: ' . $order->getId();
            }
            $messsageString .= $msg;
            $this->log->info($messsageString);
        }

        return;
    }
}
