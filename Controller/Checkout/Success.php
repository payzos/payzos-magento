<?php

namespace Payzos\PayzosMagento\Controller\Checkout;

use Magento\Sales\Model\OrderRepository;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;

class Success extends \Magento\Framework\App\Action\Action
{
    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     *
     * @var \Payzos\PayzosMagento\Lib\Payzos
     */
    private $payzos;

    /**
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Payzos\PayzosMagento\Lib\Payzos $payzos
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Payzos\PayzosMagento\Lib\Payzos $payzos
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->payzos = $payzos;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage
            ->getConfig()
            ->getTitle()
            ->set(__('Payzos payment detail'));
        return $resultPage;
    }
}
