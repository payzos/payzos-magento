# Payzos Magento Module

[Payzos](https://payzos.io) is payment service for Tezos network.

# Installation

just copy and paste this lines in your magento root

```
composer require payzos/payzos-magento
bin/magento setup:upgrade
bin/magento setup:di:compile
bin/magento c:c

```

# plugin configuration

-   open magento admin panel
-   `Stores > Configuration > Sales > Payments > PayzosMagento`
-   enable payment, fill wallet_hash with your Tezos public key, complete other inputs.

# issues

if you found any problem or you have any question please contant us.

-   [gitlab issues](https://gitlab.com/payzos/wp-payzos-payment-woocommerce/-/issues)
-   mailto : `info[at]payzos.io`
-   [Payzos.io](https://payzos.io)

# License

GPL-v3
