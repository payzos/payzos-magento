<?php

namespace Payzos\PayzosMagento\Block;

class Success extends \Magento\Framework\View\Element\Template
{
    const PATH_TO_PAYMENT_CONFIG = 'payment/payzos_magento/';

    /**
     * @var \Payzos\PayzosMagento\Lib\Payzos
     *
     */
    public $payzos;

    /**
     *
     * @return  [type]  [return description]
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Payzos\PayzosMagento\Lib\Payzos $payzos,
        array $data = []
    ) {
        $this->payzos = $payzos;
        parent::__construct($context, $data);
    }

    /**
     * @param array $_data
     * @param string $_key
     * @param string $_value
     * @param bool $_return
     *
     * @return array|null (on null will show success page with HTML output)
     */
    private function payment_success_view($_data, $_key, $_value)
    {
        if (!isset($_data['error'])) {
            $_data[$_key] = $_value;
        }
        return $_data;
    }

    public function get_data()
    {
        $data = [];
        if (!isset($_GET['payment_id'])) {
            return $this->payment_success_view($data, "error", "Not Found", true);
        }
        $payment_id = filter_var($_GET['payment_id'], FILTER_SANITIZE_STRING);
        $payment = $this->payzos->get_payment($payment_id);
        if (!$payment) {
            return $this->payment_success_view($data, "error", "Payment is unvalid", true);
        }
        if (
            !isset($payment["payment_id"]) ||
            !isset($payment["transaction_hash"]) ||
            !isset($payment["update_time"]) ||
            !isset($payment["real_amount"]) ||
            !isset($payment["status"]) ||
            !isset($payment["amount"])
        ) {
            return $this->payment_success_view($data, "error", "Payment is unvalid", true);
        }
        if ($payment["status"] !== "approved") {
            return $this->payment_success_view($data, "error", "Payment is not completed", true);
        }
        $data = $this->payment_success_view($data, 'payment_id', $payment["payment_id"]);
        $data = $this->payment_success_view(
            $data,
            'transaction_hash',
            $payment["transaction_hash"]
        );
        $date_time = date('M-D-Y h:m:s A', $payment['update_time'] - date('Z')) . ' UTC';
        $data = $this->payment_success_view($data, 'date', $date_time);
        $data = $this->payment_success_view(
            $data,
            'amount',
            $payment['real_amount'] . " | " . $payment['amount'] / 1000000 . " XTZ"
        );
        return $data;
    }
}
