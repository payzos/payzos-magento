<?php

namespace Payzos\PayzosMagento\Block;

class Redirect extends \Magento\Framework\View\Element\Template
{
    const PATH_TO_PAYMENT_CONFIG = 'payment/payzos_magento/';

    /**
     * @var \Payzos\PayzosMagento\Lib\Payzos
     *
     */
    public $payzos;

    /**
     *
     * @return  [type]  [return description]
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Payzos\PayzosMagento\Lib\Payzos $payzos,
        array $data = []
    ) {
        $this->payzos = $payzos;
        parent::__construct($context, $data);
    }

    /**
     * [back_url description]
     *
     * @param   [type]  $_order_id  [$_order_id description]
     *
     * @return  [type]              [return description]
     */
    public function back_url($_order_id)
    {
        $url = $this->getUrl('rest/V1/payzosmagento') . 'backurl';
        if (strpos($url, "?")) {
            $url .= "&order_id =" . $_order_id;
        } else {
            $url .= "?order_id =" . $_order_id;
        }
        return $url;
    }

    /**
     * [getPaymentData description]
     *
     * @return  [type]  [return description]
     */
    public function getPaymentData()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $data = [
            'wallet_hash' => $this->_scopeConfig->getValue(
                self::PATH_TO_PAYMENT_CONFIG . "wallet_hash",
                $storeScope
            ),
            'currency_code' => $this->_storeManager->getStore()->getCurrentCurrencyCode(),
        ];
        return $data;
    }

    /**
     * [getSuccessUrl description]
     *
     * @return  [type]  [return description]
     */
    public function getSuccessUrl()
    {
        return $this->getUrl('payzosmagento/checkout/success');
    }
}
