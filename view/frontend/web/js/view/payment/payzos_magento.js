define(["uiComponent", "Magento_Checkout/js/model/payment/renderer-list"], function (
    Component,
    rendererList
) {
    "use strict";
    rendererList.push({
        type: "payzos_magento",
        component: "Payzos_PayzosMagento/js/view/payment/method-renderer/payzosmagento-method",
    });
    return Component.extend({});
});
