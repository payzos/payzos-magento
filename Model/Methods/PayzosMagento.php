<?php

namespace Payzos\PayzosMagento\Model\Methods;

use Magento\Quote\Api\Data\CartInterface;
use Magento\Payment\Model\Method\AbstractMethod;

class payzosmagento extends AbstractMethod
{
    /**
     * Method code
     */
    const CODE = 'payzos_magento';

    /**
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * @param CartInterface|null $quote
     * @return bool
     */
    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        return parent::isAvailable($quote);
    }

    /**
     * @param null $storeId
     * @return bool
     */
    public function isActive($storeId = null)
    {
        return (bool) (int) $this->getConfigData('active', $storeId);
    }
}
