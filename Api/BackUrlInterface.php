<?php
namespace Payzos\PayzosMagento\Api;

interface BackUrlInterface
{
    /**
     * @param string $param
     * @return string
     */

    public function get_post($param = null);
}
